// save your token in this file
import dotenv from 'dotenv'

// require the discord.js module
import { Discord, Client } from '@typeit/discord'

dotenv.config()

// create a new Discord client
const client: Client = new Client()

// when the client is ready, run this code
// this event will only trigger one time after logging in
client.once('ready', () => {
  console.log('Ready!')
})

// login to Discord with your app's token
;(async () => await client.login(process.env.TOKEN!))()

// listening a message
client.on('message', async (message) => {
  const messageInLow = message.content
  console.log(`${message.author} says:`, message.content)

  // read message and reply with BB-8
  if (
    message.content.startsWith('!ping') ||
    message.content.endsWith('!ping')
  ) {
    await message.channel.send('beep! beep!')
  } else if (
    messageInLow.includes('hello') &&
    !messageInLow.includes('`hello`') &&
    message.author.id !== process.env.CHATBOT_ID
  ) {
    await message.channel.send(`Hello ${message.author.username}`)
  }
})
